# Sistema de seguimiento de expedientes municipales!

**Breve descripcion:**
Sistema diseñado en función de los intereses de los contribuyentes y la entidad municipal en la que se requiere realizar distintos tipos de trámites de manera rápida y efectiva donde los usuarios cuentan con la posibilidad de cargar el expediente y los movimientos efectuados según él tramite lo requiera.

# Programas requeridos


 1. Docker
Para su correcta instalacion por favor utilice el [manual de instalacion](https://docs.docker.com/get-docker/) de docker.



2. Docker Compose
Para su correcta instalacion por favor utilice el [manual de instalacion](https://docs.docker.com/compose/install/) de docker compose.


3. Git
En el caso de utilizar linux basta con el siguiente comando.
```
$ sudo apt-get install git
```
## Instalacion

1. Clonar el proyecto desde el repositorio gitlab.com con el siguiente comando.
```
$ git clone https://gitlab.com/Gaunar8/trabajo_final.git
```

2. Moverse a la carpeta del proyecto.
```
$ cd trabajo_final
```

 3. En la raiz del proyecto generar copia del archivo yml.

```
$ cp docker-compose.dist.yml docker-compose.yml
```
4. Ejecutar el siguiente comando en la carpeta raiz del proyecto.
```
$ docker-compose up
```

5. En la raiz del proyecto ejecutar el script “deploy.sh” que se encuentra en la carpeta “bin/” para que se instalen todas las dependencias del proyecto.

```
$ sh bin/deploy.sh
```

>Las dependencias se instalan en la carpeta vendor, en la medida de ejecutar correctamente la aplicación cada vez que se clone el proyecto se debe realizar este paso, las dependencias no se guardan en el repositorio por la sencilla razón de evitar el peso excesivo en la aplicación.

## Ejecucion

Para poder acceder a la aplicación es necesario ingresar en el navegador la siguiente url.
```
localhost:8000 
```
Al igual que para entrar al phpmyadmin es necesario entrar por la siguiente url.

```
localhost:8001 
```



