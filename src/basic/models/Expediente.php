<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "expediente".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $tipo_expediente_id
 * @property string|null $estado
 * @property string|null $nro_expediente
 * @property string|null $nombre
 * @property string|null $apellido
 * @property string|null $caratula
 * @property int|null $fojas
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $delete_at
 *
 * @property TipoExpediente $tipoExpediente
 * @property User $user
 * @property Movimiento[] $movimientos
 */
class Expediente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'expediente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'tipo_expediente_id', 'fojas'], 'integer'],
            [['created_at', 'updated_at', 'delete_at'], 'safe'],
            [['estado', 'nro_expediente', 'nombre', 'apellido', 'caratula'], 'string', 'max' => 255],
            [['tipo_expediente_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoExpediente::className(), 'targetAttribute' => ['tipo_expediente_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'tipo_expediente_id' => 'Tipo Expediente ID',
            'estado' => 'Estado',
            'nro_expediente' => 'Nro Expediente',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'caratula' => 'Caratula',
            'fojas' => 'Fojas',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'delete_at' => 'Delete At',
        ];
    }

    /**
     * Gets query for [[TipoExpediente]].
     *
     * @return \yii\db\ActiveQuery|TipoExpedienteQuery
     */
    public function getTipoExpediente()
    {
        return $this->hasOne(TipoExpediente::className(), ['id' => 'tipo_expediente_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Movimientos]].
     *
     * @return \yii\db\ActiveQuery|MovimientoQuery
     */
    public function getMovimientos()
    {
        return $this->hasMany(Movimiento::className(), ['expediente_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ExpedienteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExpedienteQuery(get_called_class());
    }
}
