<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property int $area_id
 * @property string|null $username
 * @property int|null $cuil
 * @property string|null $email
 * @property string|null $rol
 * @property int|null $resposable_area
 * @property string|null $password
 * @property string|null $access_token
 * @property string|null $authKey
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Area[] $areas
 * @property Expediente[] $expedientes
 * @property Movimiento[] $movimientos
 * @property Area $area
 * @property UserModuloPermiso[] $userModuloPermisos
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area_id'], 'required'],
            [['area_id', 'cuil', 'resposable_area'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'email', 'rol', 'password', 'access_token', 'authKey'], 'string', 'max' => 255],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['area_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Area ID',
            'username' => 'Username',
            'cuil' => 'Cuil',
            'email' => 'Email',
            'rol' => 'Rol',
            'resposable_area' => 'Resposable Area',
            'password' => 'Password',
            'access_token' => 'Access Token',
            'authKey' => 'Auth Key',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Areas]].
     *
     * @return \yii\db\ActiveQuery|AreaQuery
     */
    public function getAreas()
    {
        return $this->hasMany(Area::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Expedientes]].
     *
     * @return \yii\db\ActiveQuery|ExpedienteQuery
     */
    public function getExpedientes()
    {
        return $this->hasMany(Expediente::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Movimientos]].
     *
     * @return \yii\db\ActiveQuery|MovimientoQuery
     */
    public function getMovimientos()
    {
        return $this->hasMany(Movimiento::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Area]].
     *
     * @return \yii\db\ActiveQuery|AreaQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    /**
     * Gets query for [[UserModuloPermisos]].
     *
     * @return \yii\db\ActiveQuery|UserModuloPermisoQuery
     */
    public function getUserModuloPermisos()
    {
        return $this->hasMany(UserModuloPermiso::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
                $this->authKey = \Yii::$app->security->generateRandomString();
                $this->accessToken = $this->getUniqueAccessToken();
            }
            return true;
        }
        return false;
    }

    private function getUniqueAccessToken() {
        $accessToken = md5(Yii::$app->security->generateRandomString() . '_' . time());
        if ($this->findIdentityByAccessToken($accessToken)) {
            $accessToken = $this->getUniqueAccessToken();
        }
        return $accessToken;
    }
}
