<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_modulo_permiso".
 *
 * @property int $user_id
 * @property int $modulo_id
 * @property int $permiso_id
 * @property string|null $created_at
 *
 * @property Modulo $modulo
 * @property Permiso $permiso
 * @property User $user
 */
class UserModuloPermiso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_modulo_permiso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'modulo_id', 'permiso_id'], 'required'],
            [['user_id', 'modulo_id', 'permiso_id'], 'integer'],
            [['created_at'], 'safe'],
            [['user_id', 'modulo_id', 'permiso_id'], 'unique', 'targetAttribute' => ['user_id', 'modulo_id', 'permiso_id']],
            [['modulo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modulo::className(), 'targetAttribute' => ['modulo_id' => 'id']],
            [['permiso_id'], 'exist', 'skipOnError' => true, 'targetClass' => Permiso::className(), 'targetAttribute' => ['permiso_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'modulo_id' => 'Modulo ID',
            'permiso_id' => 'Permiso ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Modulo]].
     *
     * @return \yii\db\ActiveQuery|ModuloQuery
     */
    public function getModulo()
    {
        return $this->hasOne(Modulo::className(), ['id' => 'modulo_id']);
    }

    /**
     * Gets query for [[Permiso]].
     *
     * @return \yii\db\ActiveQuery|PermisoQuery
     */
    public function getPermiso()
    {
        return $this->hasOne(Permiso::className(), ['id' => 'permiso_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserModuloPermisoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserModuloPermisoQuery(get_called_class());
    }
}
