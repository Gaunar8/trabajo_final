<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Movimiento]].
 *
 * @see Movimiento
 */
class MovimientoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Movimiento[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Movimiento|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
