<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "permiso".
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $delete_at
 *
 * @property UserModuloPermiso[] $userModuloPermisos
 */
class Permiso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permiso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['created_at', 'updated_at', 'delete_at'], 'safe'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'delete_at' => 'Delete At',
        ];
    }

    /**
     * Gets query for [[UserModuloPermisos]].
     *
     * @return \yii\db\ActiveQuery|UserModuloPermisoQuery
     */
    public function getUserModuloPermisos()
    {
        return $this->hasMany(UserModuloPermiso::className(), ['permiso_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PermisoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PermisoQuery(get_called_class());
    }
}
