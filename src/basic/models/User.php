<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $access_token
 * @property datetime $token_expired
 * @property string $auth_key
 * @property string $password_hash
 */
class User extends ActiveRecord implements IdentityInterface {

    public static function tableName() {
        return 'user';
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->generateAuthKey();
                $this->setPassword($this->password);
            }
            return true;
        }
        return false;
    }

    /**
     * //Encripta el password
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = \Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generate random AuthKey
     */
    public function generateAuthKey() {
        $this->auth_key = \Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * @return Generated Token string 
     */
    public function generateToken() {
        //Si el modelo de Usuario esta cargado genera el token, lo vincula a la cuenta y devuelve el string
        $this->access_token = \Yii::$app->getSecurity()->generateRandomString();
        $this->token_expired = date('Y-m-d H:i:s', strtotime('6 hours'));
        return $this->access_token;
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::find()
                        ->where(['access_token' => $token])
                        ->andWhere(['not', ['access_token' => null]])
                        ->andWhere(['>=', 'token_expired', date('Y-m-d H:i:s')])
                        ->one();
    }

    /**
     * @return int|string current user ID
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

     /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area_id'], 'required'],
            [['area_id', 'cuil', 'resposable_area'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'email', 'rol', 'password', 'access_token', 'authKey'], 'string', 'max' => 255],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['area_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Area ID',
            'username' => 'Username',
            'cuil' => 'Cuil',
            'email' => 'Email',
            'rol' => 'Rol',
            'resposable_area' => 'Resposable Area',
            'password' => 'Password',
            'access_token' => 'Access Token',
            'authKey' => 'Auth Key',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Areas]].
     *
     * @return \yii\db\ActiveQuery|AreaQuery
     */
    public function getAreas()
    {
        return $this->hasMany(Area::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Expedientes]].
     *
     * @return \yii\db\ActiveQuery|ExpedienteQuery
     */
    public function getExpedientes()
    {
        return $this->hasMany(Expediente::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Movimientos]].
     *
     * @return \yii\db\ActiveQuery|MovimientoQuery
     */
    public function getMovimientos()
    {
        return $this->hasMany(Movimiento::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Area]].
     *
     * @return \yii\db\ActiveQuery|AreaQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    /**
     * Gets query for [[UserModuloPermisos]].
     *
     * @return \yii\db\ActiveQuery|UserModuloPermisoQuery
     */
    public function getUserModuloPermisos()
    {
        return $this->hasMany(UserModuloPermiso::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }


}
