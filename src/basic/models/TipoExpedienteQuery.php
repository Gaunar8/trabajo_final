<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoExpediente]].
 *
 * @see TipoExpediente
 */
class TipoExpedienteQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TipoExpediente[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TipoExpediente|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
