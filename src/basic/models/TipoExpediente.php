<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_expediente".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $delete_at
 *
 * @property Expediente[] $expedientes
 */
class TipoExpediente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_expediente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'delete_at'], 'safe'],
            [['nombre','descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'nombre' => 'Descripcion',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'delete_at' => 'Delete At',
        ];
    }

    /**
     * Gets query for [[Expedientes]].
     *
     * @return \yii\db\ActiveQuery|ExpedienteQuery
     */
    public function getExpedientes()
    {
        return $this->hasMany(Expediente::className(), ['tipo_expediente_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return TipoExpedienteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TipoExpedienteQuery(get_called_class());
    }
}
