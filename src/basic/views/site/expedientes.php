<?php

use yii\helpers\Html;
use yii\web\View;

$this->title = 'expedientes';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile ( "https://cdn.jsdelivr.net/npm/vue",
    ['position' => View::POS_HEAD] 
);

$this->registerJsFile ( "https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js",
    ['position' => View::POS_HEAD] 
);


?>

<div class="site-expedientes">
<div id="app">
  <h2>{{ titulo }}</h2>

    <!-- Buton de carga modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Cargar Expediente
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="exampleModalLabel">Cargar expediente</h4>
        </div>
        <form>
        <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="inputExpediente">Nro de expediente</label>
                        <input type="text" class="form-control" id="nro_expediente" placeholder="Ej 4084-5604/83">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputFojas">Nro de fojas</label>
                        <input type="number" class="form-control" id="nro_fojas" placeholder="5">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputNombre">Nombre</label>
                        <input type="text" class="form-control" id="inputNombre" placeholder="Ej Juan">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputApellido">Apellido</label>
                        <input type="text" class="form-control" id="inputApellido" placeholder="Ej Perez">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputCaratula">Caratula</label>
                        <textarea class="form-control" id="caratula" rows="3"></textarea>
                    </div>
                </div>      
                     <button class="btn btn-primary">Guardar Expediente</button>
        </div>
        </form>
    </div>
        
    </div>
    </div>

    <br><br>
    <form lass="form-control">
    <div class="form-row">
        <div class="form-group col-md-3">
        <label for="inputEmail4">Nro expediente</label>
        <input type="text" class="form-control" id="inputExpediente">
        </div>
        <div class="form-group col-md-3">
        <label for="inputApellido">Apellido</label>
        <input type="text" class="form-control" id="inputApellido">
        </div>

        <div class="form-group col-md-3">
        <label for="inputArea">Areas</label>
        <select class="form-control" id="inlineFormCustomSelect">
            <option selected>areas...</option>
            <option value="1">Contable</option>
            <option value="2">Fiscalizacion</option>
            <option value="3">Mesa de entrada</option>
        </select>
        </div>

    </div>
    </form>
    </div>

    <table class="table">
  <thead>
    <th>Exp ID</th>
    <th>Nombre</th>

    <th>Acciones</th>
  </thead>
  <tbody>
    <tr v-for="expedientes in expedientes">
      <td>{{ expedietes.id }}</td>
      <td>{{ expedientes.nombre }}</td>
      <td>
        <button class="btn btn-warning">Editar</button>
        <button class="btn btn-danger">Borrar</button>
      </td>
    </tr>
  </tbody>
</table>

</div>
<script>
 var app = new Vue({

el: "#app",
data:{
        titulo: "Expedientes"
}
})
</script>

</div>