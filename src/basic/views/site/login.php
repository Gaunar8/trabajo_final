<body>
	<div id="app"  class=" modal-dialog modal-login card" @keyup.enter="ingresar">
		<div class="modal-content">
			<div class="modal-header">
				<div class="avatar">
					<img src="https://www.tutorialrepublic.com/examples/images/avatar.png" alt="Avatar">
				</div>				
				<h4 class="modal-title">Inicio de sesión</h4>	
			</div>
			<div class="modal-body">
				<form action="/examples/actions/confirmation.php" method="post">
					<div class="form-group">
                    <label>Usuario</label>
						<input type="text" class="form-control" name="username" v-model="user.frmusername" autofocus="" required="required">		
					</div>
					<div class="form-group">
                    <label>Contraseña</label>
						<input type="password" class="form-control" name="password" v-model="user.frmpassword" required="required">	
					</div>        
					<div class="form-group">
                            <b-button block variant="success" @click="ingresar()" :disabled="cargando" v-if="mostrar">
                            <b-spinner v-if="cargando" variant="light" small></b-spinner> Entrar
                            </b-button>
                        <p class="mt-3 text-center font-weight-bold" :class="clase">{{mensaje}}</p>
                    </div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#">©2020 - Seguimiento de expedientes</a>
			</div>
		</div>
	</div>
</div>     
</body>

<script>
    //Instancia de Vue
    new Vue({
        el: "#app",
        data: {
            user: {},
            cargando: false,
            mensaje: '',
            clase: '',
            mostrar: true,
        },

        methods: {
            ingresar() {
                self = this //Guardar el objeto actual de Vue para usarlo dentro de axios
                self.cargando = true
                self.mensaje = ''
                axios({
                        method: 'POST',
                        url: '/apiv1/login',
                        data: self.user
                    })
                    .then((response) => {
                        //El resgistro de guardo correctamente
                        //Almacenar los valores devueltos por la API
                        localStorage.access_token = response.data.access_token;
                        localStorage.token_expired = response.data.token_expired;
                        localStorage.username = response.data.username;

                        //Ocultar el login
                        self.mostrar = false; //Oculta el boton
                        self.mensaje = 'Bienvenido, aguarde y será redirigido al Panel'
                        self.clase = 'text-success'
                        setTimeout(function() {
                            window.location.href = "/";
                        }, 2000);
                    })
                    .catch(function(error) {
                        //Verificar el tipo de error
                        self.clase = 'text-danger'
                        if (error.response) {
                            // Se proceso la solcitud, pero se recibio un codigo de error
                            if (error.response.status < 500) {
                                self.mensaje = 'Usuario o contraseña incorrectos'
                            } else {
                                // El error esta en el rango 5xx
                                self.mensaje = error.response.status + ': ' + error.response.statusText
                            }
                        } else if (error.request) {
                            // No se recibió respuesta desde el servidor
                            self.mensaje = 'No se pudo procesar la consulta, el servidor no responde.'
                        } else {
                            // No se puedo ejecutar la consulta
                            self.mensaje = 'Ocurrió un error, no se pudo ejecutar la consulta.'
                        }
                    })
                    .finally(function() {
                        self.cargando = false
                    });
            }
        },
    });
    //Fin Vue
</script>


<style>
.card{
box-shadow: 0 2px 3px rgba(0,0,0,0.2);
}
body {
    background-color: #f0f0f0;
	font-family: 'Varela Round', sans-serif;
}
.modal-login {		
	color: #636363;
	width: 35%;
}
.modal-login .modal-content {
	padding: 20px;
	border-radius: 5px;
	border: none;
}
.modal-login .modal-header {
	border-bottom: none;   
	position: relative;
	justify-content: center;
}
.modal-login h4 {
	text-align: center;
	font-size: 26px;
	margin: 30px 0 -15px;
}
.modal-login .form-control:focus {
	border-color: #70c5c0;
}
.modal-login .form-control, .modal-login .btn {
	min-height: 40px;
	border-radius: 3px; 
}
.modal-login .close {
	position: absolute;
	top: -5px;
	right: -5px;
}	
.modal-login .modal-footer {
	background: #ecf0f1;
	border-color: #dee4e7;
	text-align: center;
	justify-content: center;
	margin: 0 -20px -20px;
	border-radius: 5px;
	font-size: 13px;
}
.modal-login .modal-footer a {
	color: #999;
}		
.modal-login .avatar {
	position: absolute;
	margin: 0 auto;
	left: 0;
	right: 0;
	top: -70px;
	width: 95px;
	height: 95px;
	border-radius: 50%;
	z-index: 9;
	background: #60c7c1;
	padding: 15px;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
}
.modal-login .avatar img {
	width: 100%;
}
.modal-login.modal-dialog {
	margin-top: 80px;
}
.modal-login .btn, .modal-login .btn:active {
	color: #fff;
	border-radius: 4px;
	background: #60c7c1 !important;
	text-decoration: none;
	transition: all 0.4s;
	line-height: normal;
	border: none;
}
.modal-login .btn:hover, .modal-login .btn:focus {
	background: #45aba6 !important;
	outline: none;
}
.trigger-btn {
	display: inline-block;
	margin: 100px auto;
}
</style>