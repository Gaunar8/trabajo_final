<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bienvenido!!</h1>

        <p class="lead">Sistema para el seguimiento de expedientes.</p>

        <p><a class="btn btn-lg btn-success" href="/expediente">Carga tu primer expediente</a></p>
    </div>

    <div class="body-content">
    
    </div>

    </div>
</div>
