<?php

use yii\helpers\Html;
use yii\web\View;

$this->title = 'areas';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile ( "https://cdn.jsdelivr.net/npm/vue",
    ['position' => View::POS_HEAD] 
);
?>

<div id="app">
  <h2>{{ message }}</h2>

    <!-- Buton de carga modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Cargar Area
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="exampleModalLabel">Cargar Area</h4>
        </div>
        <form>
        <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputExpediente">Nombre</label>
                        <input type="text" class="form-control" id="area_nombre">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputCaratula">Caratula</label>
                        <textarea class="form-control" id="caratula" rows="3"></textarea>
                    </div>
                </div>
                <div>
                <button class="btn btn-block btn-primary">Guardar Area</button>
                </div>    
        </div>
        </form>
    </div>
        
    </div>
    </div>
    <br><br>
<form lass="form-control">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="inputEmail4">Area ID</label>
      <input type="text" class="form-control" id="inputExpediente">
    </div>
    <div class="form-group col-md-3">
      <label for="inputApellido">Nombre</label>
      <input type="text" class="form-control" id="inputApellido">
    </div>
  </div>
</form>
    <div class="row">
<table class="table">
  <thead>
    <th>Area ID</th>
    <th>Nombre</th>
    <th>Acciones</th>
  </thead>
  <tbody>
    <tr v-for="area in areas">
      <td>{{ area.id }}</td>
      <td>{{ area.nombre }}</td>
      <td>
        <button class="btn btn-warning">Editar</button>
        <button class="btn btn-danger">Borrar</button>
      </td>
    </tr>
  </tbody>
</table>
</div>
</div>


<script>
var app = new Vue({
  el: '#app',
  data: {
    message: 'Areas!',
    areas: [
      {id:'1' ,nombre: 'Documentación'},
      {id:'2' ,nombre: 'Recepción'},
      {id:'3' ,nombre: 'Mantenimiento'},
      {id:'4' ,nombre: 'Mesa de Ayuda'},
      {id:'5' ,nombre: 'Call Center'},
      {id:'6' ,nombre: 'Informatica'},
      {id:'7' ,nombre: 'Administración'},
    ]
  }
})
</script>
</div>