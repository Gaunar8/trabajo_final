<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\VueAsset;
use app\widgets\Alert;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$this->registerCssFile("//unpkg.com/bootstrap/dist/css/bootstrap.min.css",['position'=>$this::POS_HEAD]);
$this->registerCssFile("//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css",['position'=>$this::POS_HEAD]);
//$this->registerCssFile("https://unpkg.com/font-awesome@4.7.0/css/font-awesome.css",['position'=>$this::POS_HEAD]);
$this->registerCssFile("https://use.fontawesome.com/releases/v5.0.13/css/all.css",['position'=>$this::POS_HEAD]);


$this->registerJsFile("https://unpkg.com/sweetalert2@7.26.29/dist/sweetalert2.all.min.js",['position'=>$this::POS_HEAD]);
$this->registerJsFile("https://cdn.jsdelivr.net/npm/vue/dist/vue.js",['position'=>$this::POS_HEAD]);
$this->registerJsFile("https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js",['position'=>$this::POS_HEAD]);
$this->registerJsFile("https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js",['position'=>$this::POS_HEAD]);
$this->registerJsFile("https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js",['position'=>$this::POS_HEAD]);


//Redireccionar al Login cuando no existe un token
$this->registerJs('
    //Verifica que exista el token almacenado
    if (!localStorage.access_token && window.location.pathname !== "/login") {
        window.location.href = "/login";
    } else {
        //Si no hubo redirección muestra el contenido y el nombre de usuario
        document.body.style.display="block";
        appUsername = document.getElementById("logout");
        appUsername.textContent = " " + localStorage.username + " (Salir)";
    }
');

$this->registerJs('   
    //Eliminar datos de sesion 
    function logout() {
        localStorage.clear();
        window.location.href = "/login";
    }
', \yii\web\View::POS_HEAD);

//Registrar CSS y JS para Vue y Bootstrap 4
VueAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body style="display: none;">
        <?php $this->beginBody() ?>
        <div class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="navbar navbar-nav w-100 justify-content-end" id="navbar" style="display: <?= (Yii::$app->request->url != '/login') ? 'block' : 'none' ?>">
            <div class="container" >
                <?= Html::a(' Inicio', ['/'], ['class' => 'fas fa-home text-white']) ?>
                <?= Html::a(' Expediente', ['/expediente'], ['class' => 'fas fa-book text-white']) ?>
                <?= Html::a(' Tipoexpediente', ['/tipoexpediente'], ['class' => 'fas fa-clipboard-list text-white']) ?>
                <?= Html::a(' Area', ['/area'], ['class' => 'fas fa-object-group text-white']) ?>
                <?= Html::a(' Usuarios', ['/user'], ['class' => 'fas fa-user text-white']) ?>
                <?= Html::a('', ['#'], ['id' => 'logout','class'=>'fas fa-power-off text-white', 'onclick' => 'logout()']) ?>

            </div>
        </div>
        </div>

        <div class="wrap">
            <div class="">
                <?= $content ?>
            </div>
        </div>

        <?php $this->beginBody() ?>

        <?php $this->endBody() ?>
    </body>

</html>
<?php $this->endPage() ?>