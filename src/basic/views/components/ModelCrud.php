<script type="text/x-template" id="crud-template">
    <div class="container">
        <br>
        <h1 class="text-capitalize "><span v-html="icon"></span> {{crudname}}</h1>
        <b-modal v-model="modalShow"  id="modal-1" v-bind:title="isNewRecord ? 'Crear ' + crudname : 'Editar ' + crudname" value="">
            <div>
                <form action="">
                    <div v-if="areas" class="form-group">
                        <label>Areas</label>
                        <select class="form-control" name="" id="">
                        <option >--</option>
                        <option v-bind:selected="activemodel.area_id == area.id" v-for="area in areas" v-on:click="setArea(area.id)">{{area.nombre}}</option>
                        </select>
                        <span class="text-danger" v-if="errors.area_id" >{{errors.area_id}}</span>
                    </div>
                    <div v-if="tipos_expedientes" class="form-group">
                        <label>Tipo Expediente</label>
                        <select class="form-control" name="" id="">
                        <option>--</option>
                        <option  v-for="tipo in tipos_expedientes" v-bind:selected="activemodel.tipo_expediente_id == tipo.id" v-on:click="setTipoExpediente(tipo.id)">{{tipo.nombre}}</option>
                        </select>
                        <span class="text-danger" v-if="errors.tipo_expediente_id" >{{errors.tipo_expediente_id}}</span>
                    </div>
                    <div v-if="i>=0" v-for="(field,i) in modelfields" class="form-group">
                        <label :for="field">{{field}}</label>
                        <input v-model="activemodel[field]" type="text" :name="field" :id="field" class="form-control" :placeholder="'Ingrese '+ field " aria-describedby="helpId">
                        <span class="text-danger" v-if="errors[field]" >{{errors[field]}}</span>
                    </div>
                </form>
            </div>
            <template v-slot:modal-footer="{ ok, cancel, hide }">
                <button v-if="isNewRecord"  @click="addModel()" type="button" class="btn btn-primary m-3">Guardar {{modelname}}</button>
                <button v-if="!isNewRecord" @click="updateModel(activemodel.id)" type="button" class="btn btn-primary m-3">Actualizar {{modelname}}</button>
            </template>

        </b-modal>
        <p>
            <b-button  v-on:click="crearModel" variant="success" class=" fas fa-plus-square"> Crear {{crudname}}</b-button>
        </p>

        <div class="table-responsive">

            <table class="table table-striped table-sm" id="myTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th v-for="field in modelfields">{{field}}</th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td></td>
                    <td v-for="field in modelfields">
                        <input v-on:change="getModels()" class="form-control" v-model="filter[field]">
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(model,key) in models" v-bind:key="model[modelfields[0]]">
                    <td>{{key+1}}</td>
                    <td v-for="field in modelfields">{{model[field]}}</td>
                    <td>
                       <b-button v-b-modal.modal-1 v-on:click="editModel(key)" type="button" class="btn btn-warning fas fa-edit"></b-button>
                    </td>
                    
                    <td>
                       <b-button v-on:click="deleteModel(model.id)" type="button"  class="btn btn-danger fas fa-trash-alt"></b-button>
                    </td>
                    <td v-if="modelname === 'expediente'">
                        <b-dropdown text="Movimientos" variant="info" class="">
                            <b-dropdown-item @click="crearMovimiento(model.id)"  href="#"><i class="fas fa-plus-square"> Crear</i></b-dropdown-item>
                            <b-dropdown-item @click="listarMovimiento(model.id)" href="#"><i class="fas fa-eye"> Listado</i></b-dropdown-item>
                        </b-dropdown>
                    </td>
                </tr>

                </tbody>
                
            </table>
            <b-pagination
            v-model="currentPage"
            :total-rows.number="pagination.total"
            :per-page.number="pagination.perPage"
            aria-controls="my-table"
        ></b-pagination>
        <p>{{currentPage}}</p>
        </div>
        <modal-movimiento v-if="iniciarMovimiento" :expediente_id="expediente_id" 
        :areas_list="areas_list" :show="iniciarMovimiento" @close="iniciarMovimiento = false">
        </modal-movimiento>
        <listado-movimiento v-if="show_movimiento_listado" :expediente_id="expediente_id" 
        @close="show_movimiento_listado = false">
        </listado-movimiento>
    </div>
</script>
<script>
    const Crud = {
        name: 'crud',
        template: '#crud-template',
        props: {
            modelname: String,
            crudname: String,
            model : Object,
            icon: String,
            fields: {
                type:Array,
                // default: Object.keys(model),
            },
        },
        mounted() {
            this.getModels();
            if(this.modelname == 'expediente'){
                this.getTiposExpedientes();
                
            }else{
                this.tipos_expedientes = null;
            }
            if(this.modelname == 'user'){
                this.getAreas();
            }else{
                this.areas = null;
            }
            this.getAreasForMovimientos();
        },
        watch:{
            currentPage: function() {
                this.getModels();
            }
        },
        data : function(){
            return {
                modalShow: false,
                modelfields: this.fields??Object.keys(this.model),
                currentPage: 1,
                pagination:{},  
                filter:{},
                errors: {},
                models: [],
                areas: [],
                tipos_expedientes: [],
                activemodel:{},
                isNewRecord:true,
                iniciarMovimiento: false,
                expediente_id : null,
                areas_list : [],
                show_movimiento_listado: false,
            }
        },
        methods: {
            crearMovimiento: function(id){
                this.iniciarMovimiento = true;
                this.expediente_id = id;
            },
            listarMovimiento: function(id){
                this.show_movimiento_listado = true;
                this.expediente_id = id;
            },
            getAreasForMovimientos: function(){
                that = this
                axios.get('/apiv1/area')
                    .then(function (response) {
                        that.areas_list = response.data;
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
            getAreas: function(){
                that = this
                axios.get('/apiv1/area')
                    .then(function (response) {
                        that.areas = response.data;
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
            getTiposExpedientes: function(){
                that = this
                axios.get('/apiv1/tipoexpediente')
                    .then(function (response) {
                        that.tipos_expedientes = response.data;
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
            setArea: function(id){
                console.log('seteando area');
                this.activemodel.area_id = id;
            },
            setTipoExpediente: function(id){
                this.activemodel.tipo_expediente_id = id;
            },
            crearModel: function(){
                this.isNewRecord = true;
                this.activemodel = {};
                this.modalShow = true;
            },

            normalizeErrors: function(errors){
                var allErrors = {};
                for(var i = 0 ; i < errors.length; i++ ){
                    allErrors[errors[i].field] = errors[i].message;
                }
                return allErrors;
            },

            getModels: function(){
                var self = this;
                self.errors = {};
                axios.get('/apiv1/'+self.modelname+'?page='+self.currentPage+'&per-page=10',{params:self.filter})
                    .then(function (response) {

                        self.pagination.total = response.headers['x-pagination-total-count'];
                        self.pagination.totalPages = response.headers['x-pagination-page-count'];
                        self.pagination.perPage = response.headers['x-pagination-per-page'];
                        self.models = response.data;
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
            deleteModel: function(id){
                Swal.fire({
                type: 'warning',
                title: 'Estas seguro?',
                text: "¡No podrás revertir esto!",
                
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Sí, bórralo!',
                cancelButtonText: 'Cancelar'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Eliminado!',
                    'el registro numero '+id+' ha sido eliminado.',
                    'success'
                    );
                    var self = this;
                    axios.delete('/apiv1/'+self.modelname+'/'+id,{})
                    .then(function (response) {
                        // handle success
                        self.getModels();
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                        Swal.fire(
                        'Error eliminar!',
                        'el registro numero '+id+' no se ha sido eliminado.',
                        'error')
                    })
                    .then(function () {
                        // always executed
                        
                    }); 
                }
                })
            },

            editModel: function (key) {
                this.activemodel = Object.assign({},this.models[key]);
                console.log(this.activemodel);
                // this.activemodel.key = key;
                this.isNewRecord = false;
            },
            addModel: function(){
                var self = this;
                axios.post('/apiv1/'+self.modelname,self.activemodel)
                    .then(function (response) {
                        // handle success
                        console.log(response.data);
                        self.getModels()
                        self.modalShow = false;
                        self.activemodel = {};
                        Swal.fire(
                        'Registro guardado!',
                        'Haz clic en el botón!',
                        'success'
                        )
                    })

                    .catch(function (error) {
                        // var errors = error.response.data;
                        console.log(error.response.data);
                        self.errors = self.normalizeErrors(error.response.data);
                        // handle error
                        console.log(self.errors);

                        Swal.fire(
                        'Error al guardar!',
                        'Haz clic en el botón!',
                        'error'
                        )
                    
                    })

                    .then(function () {
                        // always executed
                    });
            },
        
            updateModel: function (key) {
                Swal.fire({
                type: 'warning',
                title: 'Estas seguro?',
                
                showCancelButton: true,
                confirmButtonColor: '#03ab00',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Sí, Editarlo!',
                cancelButtonText: 'Cancelar'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Actualizado!',
                    'el id numero '+key+' ha sido actualizado.',
                    'success'
                    );
                var self = this;
                axios.patch('/apiv1/'+self.modelname+"/"+key,self.activemodel)
                    .then(function (response) {
                        // handle success
                        //self.getModel();
                        //self.activemodel = {};
                        self.activemodel = Object.assign({},self.models[response.data.id]);
                        self.isNewRecord = true;
                        self.modalShow = false;
                        self.getModels();
                        self.activemodel = {};
   
                    })

                    .catch(function (error) {
                        // handle error
                        console.log(error);
                        self.errors = self.normalizeErrors(error.response.data);
                        Swal.fire(
                        'No pudo ser editado!',
                        'Haz clic en el botón!',
                        'success'
                        )
                    })
                    .then(function () {
                        // always executed
                    }); 
                }
                })
            },

        }
    }
    
</script>