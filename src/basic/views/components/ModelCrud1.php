<script type="text/x-template" id="crud-template">
<h2>{{modelname}}</h2>
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Header</th>
              <th>Header</th>
              <th>Header</th>
              <th>Header</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1,001</td>
              <td>Lorem</td>
              <td>ipsum</td>
              <td>dolor</td>
              <td>sit</td>
            </tr>
            <tr>
              <td>1,002</td>
              <td>amet</td>
              <td>consectetur</td>
              <td>adipiscing</td>
              <td>elit</td>
            </tr>
            <tr>
              <td>1,003</td>
              <td>Integer</td>
              <td>nec</td>
              <td>odio</td>
              <td>Praesent</td>
            </tr>
            <tr>
              <td>1,003</td>
              <td>libero</td>
              <td>Sed</td>
              <td>cursus</td>
              <td>ante</td>
            </tr>
            <tr>
              <td>1,004</td>
              <td>dapibus</td>
              <td>diam</td>
              <td>Sed</td>
              <td>nisi</td>
            </tr>
            <tr>
              <td>1,005</td>
              <td>Nulla</td>
              <td>quis</td>
              <td>sem</td>
              <td>at</td>
            </tr>
            <tr>
              <td>1,006</td>
              <td>nibh</td>
              <td>elementum</td>
              <td>imperdiet</td>
              <td>Duis</td>
            </tr>
            <tr>
              <td>1,007</td>
              <td>sagittis</td>
              <td>ipsum</td>
              <td>Praesent</td>
              <td>mauris</td>
            </tr>
            <tr>
              <td>1,008</td>
              <td>Fusce</td>
              <td>nec</td>
              <td>tellus</td>
              <td>sed</td>
            </tr>
            <tr>
              <td>1,009</td>
              <td>augue</td>
              <td>semper</td>
              <td>porta</td>
              <td>Mauris</td>
            </tr>
            <tr>
              <td>1,010</td>
              <td>massa</td>
              <td>Vestibulum</td>
              <td>lacinia</td>
              <td>arcu</td>
            </tr>
            <tr>
              <td>1,011</td>
              <td>eget</td>
              <td>nulla</td>
              <td>Class</td>
              <td>aptent</td>
            </tr>
            <tr>
              <td>1,012</td>
              <td>taciti</td>
              <td>sociosqu</td>
              <td>ad</td>
              <td>litora</td>
            </tr>
            <tr>
              <td>1,013</td>
              <td>torquent</td>
              <td>per</td>
              <td>conubia</td>
              <td>nostra</td>
            </tr>
            <tr>
              <td>1,014</td>
              <td>per</td>
              <td>inceptos</td>
              <td>himenaeos</td>
              <td>Curabitur</td>
            </tr>
            <tr>
              <td>1,015</td>
              <td>sodales</td>
              <td>ligula</td>
              <td>in</td>
              <td>libero</td>
            </tr>
          </tbody>
        </table>
      </div>
      </script>
<script>
    const Crud = {
        name: 'crud',
        template: '#crud-template',
        props: {
            modelname: String,
            model : Object,
            fields: {
                type:Array,
                // default: Object.keys(model),
            },
        },
        mounted() {
            this.getModels();
        },
        watch:{
            currentPage: function() {
                this.getModels();
            }
        },
        data : function(){
            return {
                modalShow: false,
                modelfields: this.fields??Object.keys(this.model),
                currentPage: 1,
                pagination:{},  
                filter:{},
                errors: {},
                models: [],
                activemodel:{},
                isNewRecord:true,
            }
        },
        methods: {
            normalizeErrors: function(errors){
                var allErrors = {};
                for(var i = 0 ; i < errors.length; i++ ){
                    allErrors[errors[i].field] = errors[i].message;
                }
                return allErrors;
            },
            getModels: function(){
                var self = this;
                self.errors = {};
                axios.get('/apiv1/'+self.modelname+'?page='+self.currentPage,{params:self.filter})
                    .then(function (response) {
                        self.pagination.total = response.headers['x-pagination-total-count'];
                        self.pagination.totalPages = response.headers['x-pagination-page-count'];
                        self.pagination.perPage = response.headers['x-pagination-per-page'];
                        self.models = response.data;
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
            deleteModel: function(id){
                Swal.fire({
                type: 'warning',
                title: 'Estas seguro?',
                text: "¡No podrás revertir esto!",
                
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Sí, bórralo!'
                }).then((result) => {
                if (result.value) {
                    Swal.fire(
                    'Eliminado!',
                    'el registro numero '+id+' ha sido eliminado.',
                    'success'
                    );
                    var self = this;
                    axios.delete('/apiv1/'+self.modelname+'/'+id,{})
                    .then(function (response) {
                        // handle success
                        self.getModels();
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                        Swal.fire(
                        'Error eliminar!',
                        'el registro numero '+id+' no se ha sido eliminado.',
                        'error')
                    })
                    .then(function () {
                        // always executed
                    }); 
                }
                })
            },
            editModel: function (key) {
                this.activemodel = Object.assign({},this.models[key]);
                console.log(this.activemodel);
                // this.activemodel.key = key;
                this.isNewRecord = false;
            },
            addModel: function(){
                var self = this;
                axios.post('/apiv1/'+self.modelname,self.activemodel)
                    .then(function (response) {
                        // handle success
                        console.log(response.data);
                        self.getModels()
                        self.modalShow = false;
                        self.activemodel = {};
                        Swal.fire(
                        'Expediente guardado!',
                        'Haz clic en el botón!',
                        'success'
                        )
                    })
                    .catch(function (error) {
                        // var errors = error.response.data;
                        console.log(error.response.data);
                        self.errors = self.normalizeErrors(error.response.data);
                        // handle error
                        console.log(self.errors);

                        Swal.fire(
                        'Error al guardar Expediente!',
                        'Haz clic en el botón!',
                        'error'
                        )
                    })
                    .then(function () {
                        // always executed
                    });
            },
            updateModel: function (key) {
                var self = this;
                axios.patch('/apiv1/'+self.modelname+"/"+key,self.activemodel)
                    .then(function (response) {
                        // handle success
                        //self.getModel();
                        //self.activemodel = {};
                        self.activemodel = Object.assign({},self.models[response.data.id]);
                        self.isNewRecord = true;
                        self.modalShow = false;
                        self.getModels();
                        self.activemodel = {};
                        Swal.fire(
                        'Expediente editado con exito!',
                        'Haz clic en el botón!',
                        'success'
                        )
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                        self.errors = self.normalizeErrors(error.response.data);
                        Swal.fire(
                        'Expediente no pudo ser editado!',
                        'Haz clic en el botón!',
                        'success'
                        )
                    })
                    .then(function () {
                        // always executed
                    });
            }

        }
    }
    $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>