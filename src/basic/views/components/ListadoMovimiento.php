<script type="text/x-template" id="modal-listado-template">

<div id="listado-movimiento">
<b-modal id="listado_movimiento" v-model="listShow" :no-close-on-esc="true" :no-close-on-backdrop="true" >
<template v-slot:modal-header="">
<h3>Listado de Movimientos</h3>
</template>
<table class="table table-striped table-sm" id="myTable">
    <thead>
    <tr>
        <th>Area Origen</th>
        <th>Area Destino</th>
        <th>Motivo</th>
        <th>Fecha de Inicio</th>
        <th>Fecha de Fin</th>
        <th>Usuario</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for="movimiento in movimientos">
    <td>Area Origen</td>
    <td>{{ movimiento.area_id }}</td>
    <td>{{ movimiento.motivo }}</td>
    <td>{{ movimiento.fecha_inicio }}</td>
    <td>{{ movimiento.fecha_fin }}</td>
    <td>{{ movimiento.user_id }}</td>
    </tr>

    </tbody>
    
</table>
<template v-slot:modal-footer="{ ok, cancel, close }">
                <button @click="closeModal()" type="button" class="btn btn-secondary m-3">Cerrar</button>
            </template>
<b-pagination
v-model="currentPage"
:total-rows.number="pagination.total"
:per-page.number="pagination.perPage"
aria-controls="myTable"
></b-pagination>
</b-modal>
</div>
</script>