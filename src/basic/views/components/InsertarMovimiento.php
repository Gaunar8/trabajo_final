<script type="text/x-template" id="modal-movimiento-template">
        <div>
        <b-modal v-model="show" id="modal-movimientos" 
        :no-close-on-esc="true" :no-close-on-backdrop="true">
        <template v-slot:modal-header="">
            <h3>Cargar Movimientos</h3>
        </template>
            <form action="">
                <div>
                <label for="">Nombre Completo</label>
                <input disabled class="form-control" type="text" v-model="nombreCompleto">
                <label for="">Número de Expediente</label>
                <input disabled class="form-control" type="text" v-model="expediente.nro_expediente">
                <label for="">Caratula</label>
                <input disabled class="form-control" type="text" v-model="expediente.caratula">
                <label for="">Fojas</label>
                <input disabled class="form-control" type="text" v-model="expediente.fojas">
                <label for="">Fecha Inicio</label>
                <input class="form-control" type="date" v-model="movimiento.fecha_inicio">
                <label for="">Fecha Fin</label>
                <input class="form-control" type="date" v-model="movimiento.fecha_fin">
                <label for="">Motivo</label>
                <input class="form-control" type="text" v-model="movimiento.motivo">
                </div>
                <div v-if="areas_list" class="form-group">
                    <label>Areas de Destino</label>
                    <select class="form-control" id="">
                    <option >--</option>
                    <option v-for="area in areas_list" @click="seleccionarArea(area.id)" id="area.id">{{ area.nombre }}</option>
                    </select>
                </div>
            </form>
            <template v-slot:modal-footer="{ ok, cancel, close }">
                <button @click="addMovimiento()" type="button" class="btn btn-primary m-3">Guardar</button>
                <button @click="closeModal()" type="button" class="btn btn-secondary m-3">Cancelar</button>
            </template>

        </b-modal>
        </div>
</script>