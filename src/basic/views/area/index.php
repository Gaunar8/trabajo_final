<?php

echo $this->render('/components/ModelCrud');

?>



<div id="app">
    <crud
        v-bind:model="model"
        v-bind:fields="['user_id','nombre','descripcion']"
        v-bind:modelname="modelname"
        v-bind:icon="icon"
        v-bind:crudname="crudname"
    ></crud>
</div>

<script>

    var app = new Vue({
        el: "#app",
        components:{
            crud: Crud,
        },
        data:{
            model: <?= json_encode($model->getAttributes()) ?>,
            //rules: <?//= json_encode($model->getRelatedRecords()) ?>//,
            //relations: <?//= json_encode($model->rules()) ?>//,
            fields:null, // poner en un array los campos que queres
            modelname: <?= json_encode($model->endpointName())?>,
            icon: <?= json_encode($model->getIcon())?>,
            crudname: <?= json_encode($model->getCrudName())?>,
        }
    })
</script>
