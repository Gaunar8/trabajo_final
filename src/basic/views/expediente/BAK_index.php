<?php
echo $this->render('/components/ModelCrud');
?>
<div id="app">
    <crud
        v-bind:model="model"
        v-bind:fields="['nro_expediente','fojas','nombre','apellido','caratula']"
        v-bind:modelname="modelname"
        v-bind:icon="icon"
        v-bind:crudname="crudname"
    ></crud>
</div>
<script>
    var app = new Vue({
        el: "#app",
        components:{
            crud: Crud,
        },
        data:{
            model: <?= json_encode($model->getAttributes()) ?>,
            fields:null, // poner en un array los campos que queres
            modelname: <?= json_encode($model->endpointName())?>,
            icon: <?= json_encode($model->getIcon())?>,
            crudname: <?= json_encode($model->getCrudName())?>,
        }
    })
</script>