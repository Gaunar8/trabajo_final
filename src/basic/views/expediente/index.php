<?php
echo $this->render('/components/ModelCrud');
echo $this->render('/components/InsertarMovimiento');
echo $this->render('/components/ListadoMovimiento');
?>
<div id="app">
    <crud
        v-bind:model="model"
        v-bind:fields="['nro_expediente','fojas','nombre','apellido','caratula']"
        v-bind:modelname="modelname"
        v-bind:icon="icon"
        v-bind:crudname="crudname"
    ></crud>
</div>
<script>

Vue.component('listado-movimiento', {
    template: '#modal-listado-template',
    props: {
        expediente_id: null,
    },
    data: function(){
        return {
        listShow: true,
        movimientos: [],
        currentPage: 1,
        pagination:{},  
        filter:{},
        }
    },
    mounted() {
        console.log(this.currentPage);
        this.getMovimientos();
    },
    methods: {
        closeModal: function(){
                this.$emit('close');
            },
        getMovimientos: function(){
                var self = this;
                self.errors = {};
                axios.get('/apiv1/expediente/'+self.expediente_id,
                {
                    params:{
                        'expand': 'movimientos',
                        'page': self.currentPage,
                        'per-page': '1',
                    }
                })
                    .then(function (response) {
                        self.pagination.total = response.headers['x-pagination-total-count'];
                        self.pagination.totalPages = response.headers['x-pagination-page-count'];
                        self.pagination.perPage = response.headers['x-pagination-per-page'];
                        self.movimientos = response.data.movimientos;
                    })
                    
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
    }
})

Vue.component('modal-movimiento', {
        template: '#modal-movimiento-template',
        props: {
            areas_list: Array,
            expediente_id: null,
        },
        data: function(){
            return {
                show: true,
                expediente: Object,
                movimiento: {
                    expediente_id: this.expediente_id,
                    user_id: null,
                    area_id: null,
                    fecha_inicio: '',
                    fecha_fin: '',
                    },
                }
        },
        mounted() {
            this.getExpediente();
            this.getUser();
        },
        computed: {
            nombreCompleto: function(){
                return this.expediente.apellido + ' ' + this.expediente.nombre;
            }
        },
        methods: {
            closeModal: function(){
                this.$emit('close');
            },
            addMovimiento: function(){
                var that = this;
                axios.post('/apiv1/movimiento',that.movimiento)
                    .then(function (response) {
                        // handle success
                        console.log(response.data);
                        that.$emit('close');
                        that.movimiento = {};
                        Swal.fire(
                        'Registro guardado!',
                        'Haz clic en el botón!',
                        'success'
                        )
                    })

                    .catch(function (error) {
                        // var errors = error.response.data;
                        console.log(error.response.data);
                        // handle error
                        console.log(that.errors);

                        Swal.fire(
                        'Error al guardar!',
                        'Haz clic en el botón!',
                        'error'
                        )
                    
                    })

                    .then(function () {
                        // always executed
                    });
            },
            seleccionarArea: function(id){
                that = this;
                that.movimiento.area_id = id;
            },
            getUser: function(){
                let username = localStorage.getItem('username');
                var that = this;
                that.errors = {};
                axios.get('/apiv1/user/', {
                    params:{
                        username: username,
                    }
                })
                    .then(function (response) {
                        that.movimiento.user_id = response.data[0].id;
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
            getExpediente: function(){
                var self = this;
                self.errors = {};
                axios.get('/apiv1/expediente/'+self.expediente_id)
                    .then(function (response) {
                        self.expediente = response.data;
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            },
        }
    })
    var app = new Vue({
        el: "#app",
        components:{
            crud: Crud,
        },
        data:{
            model: <?= json_encode($model->getAttributes()) ?>,
            fields:null, // poner en un array los campos que queres
            modelname: <?= json_encode($model->endpointName())?>,
            currentPage: 1, //Paginacion, pagina actual
            records: 1, // //Paginacion, total de registros
            listado: null, //Respuesta de la api
            search: null, //Filtro por nombre
            icon: <?= json_encode($model->getIcon())?>,
            crudname: <?= json_encode($model->getCrudName())?>,
        }
    })
</script>