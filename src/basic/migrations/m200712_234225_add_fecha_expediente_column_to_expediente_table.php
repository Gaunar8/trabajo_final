<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%expediente}}`.
 */
class m200712_234225_add_fecha_expediente_column_to_expediente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%expediente}}', 'fecha_expediente', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%expediente}}', 'fecha_expediente');
    }
}
