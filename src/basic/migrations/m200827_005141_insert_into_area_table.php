<?php

use yii\db\Migration;

/**
 * Class m200827_005141_insert_into_area_table
 */
class m200827_005141_insert_into_area_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%area}}', [
            'nombre' => 'admin'
        ]);
    }

}
