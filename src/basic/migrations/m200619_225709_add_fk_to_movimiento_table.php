<?php

use yii\db\Migration;

/**
 * Class m200619_225709_add_fk_to_movimiento_table
 */
class m200619_225709_add_fk_to_movimiento_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `author_id`
        $this->createIndex(
            'idx-movimiento-expediente_id',
            'movimiento',
            'expediente_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-movimiento-expediente_id',
            'movimiento',
            'expediente_id',
            'expediente',
            'id',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            'idx-movimiento-user_id',
            'movimiento',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-movimiento-user_id',
            'movimiento',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            'idx-movimiento-area_id',
            'movimiento',
            'area_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-movimiento-area_id',
            'movimiento',
            'area_id',
            'area',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         // drops foreign key for table `movimiento`
         $this->dropForeignKey(
            'fk-movimiento-area_id',
            'movimiento'
        );

        // drops index for column `area_id`
        $this->dropIndex(
            'idx-movimiento-area_id',
            'movimiento'
        );

        // drops foreign key for table `movimiento`
        $this->dropForeignKey(
            'fk-movimiento-user_id',
            'movimiento'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-movimiento-user_id',
            'movimiento'
        );

        // drops foreign key for table `movimiento`
        $this->dropForeignKey(
            'fk-movimiento-expediente_id',
            'movimiento'
        );

        // drops index for column `expediente_id`
        $this->dropIndex(
            'idx-movimiento-expediente_id',
            'movimiento'
        );

    }
}
