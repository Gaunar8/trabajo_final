<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%usuario}}`.
 */
class m200608_140358_create_usuario_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'area_id' => $this->integer()->notNull(),
            'username' => $this->string(),
            'cuil' => $this->integer(),
            'email' => $this->string(),
            'rol'=> $this->string(),
            'resposable_area' => $this->boolean()->defaultValue(false),
            'password' => $this->string(),
            'accessToken' => $this->string(),
            'authKey'=> $this->string(),
            'created_at'=>$this->datetime(),
            'updated_at'=>$this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}