<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_and_modulo_permiso}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_and_modulo}}`
 * - `{{%permiso}}`
 */
class m200610_013904_create_junction_table_for_user_and_modulo_and_permiso_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_modulo_permiso}}', [
            'user_id' => $this->integer(),
            'modulo_id' => $this->integer(),
            'permiso_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'PRIMARY KEY(user_id, modulo_id, permiso_id)',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_modulo_permiso-user_id}}',
            '{{%user_modulo_permiso}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_modulo_permiso-user_id}}',
            '{{%user_modulo_permiso}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `modulo_id`
        $this->createIndex(
            '{{%idx-user_modulo_permiso-modulo_id}}',
            '{{%user_modulo_permiso}}',
            'modulo_id'
        );

        // add foreign key for table `{{%modulo}}`
        $this->addForeignKey(
            '{{%fk-user_modulo_permiso-modulo_id}}',
            '{{%user_modulo_permiso}}',
            'modulo_id',
            '{{%modulo}}',
            'id',
            'CASCADE'
        );

        // creates index for column `permiso_id`
        $this->createIndex(
            '{{%idx-user_modulo_permiso-permiso_id}}',
            '{{%user_modulo_permiso}}',
            'permiso_id'
        );

        // add foreign key for table `{{%permiso}}`
        $this->addForeignKey(
            '{{%fk-user_modulo_permiso-permiso_id}}',
            '{{%user_modulo_permiso}}',
            'permiso_id',
            '{{%permiso}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user_and_modulo}}`
        $this->dropForeignKey(
            '{{%fk-user_modulo_permiso-permiso_id}}',
            '{{%user_modulo_permiso}}'
        );

        // drops index for column `modulo_id`
        $this->dropIndex(
            '{{%idx-user_modulo_permiso-permiso_id}}',
            '{{%user_modulo_permiso}}'
        );


        // drops foreign key for table `{{%modulo}}`
        $this->dropForeignKey(
            '{{%fk-user_modulo_permiso-modulo_id}}',
            '{{%user_modulo_permiso}}'
        );

        // drops index for column `modulo_id`
        $this->dropIndex(
            '{{%idx-user_modulo_permiso-modulo_id}}',
            '{{%user_modulo_permiso}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_modulo_permiso-user_id}}',
            '{{%user_modulo_permiso}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_modulo_permiso-user_id}}',
            '{{%user_modulo_permiso}}'
        );

        $this->dropTable('{{%user_modulo_permiso}}');
    }
}
