<?php

use yii\db\Migration;

/**
 * Class m200619_225810_add_fk_to_user_table
 */
class m200619_225810_add_fk_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         // creates index for column `area_id`
         $this->createIndex(
            'idx-user-area_id',
            'user',
            'area_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-area_id',
            'user',
            'area_id',
            'area',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-area_id',
            'user'
        );

        // drops index for column `area_id`
        $this->dropIndex(
            'idx-user-area_id',
            'user'
        );
    }
}
