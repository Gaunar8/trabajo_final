<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%expediente}}`.
 */
class m200608_021244_create_expediente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {$this->createTable('{{%expediente}}', [
        'id' => $this->primaryKey(),
        'user_id' => $this->integer(),
        'tipo_expediente_id' => $this->integer(),
        //'titulo' => $this->string()->notNull(),
        'estado' => $this->string(),
        'nro_expediente' => $this->string(),
        'nombre' => $this->string(),
        'apellido' => $this->string(),
        'caratula' => $this->string(),
        'fojas' => $this->integer(11),
        'created_at' => $this->datetime(),
        'updated_at' => $this->datetime(),
        'delete_at' => $this->datetime(),
    ]);
    /* // crea indice para columna `user_id`a
    $this->createIndex(
        'idx-expediente-user_id',
        'expediente',
        'user_id'
    );

    // crea la clave foranea para la tabla `user`
    $this->addForeignKey(
        'fk-expediente-user_id',
        'expediente',
        'user_id',
        'user',
        'id'
    );
    

    // crea indice para columna `tipo_expediente_id`
    $this->createIndex(
        'idx-expediente-tipo_expediente_id',
        'expediente',
        'tipo_expediente_id'
    );

    // agrega clave foranea para tabla `tipo_expediente`
    $this->addForeignKey(
        'fk-expediente-tipo_expediente_id',
        'expediente',
        'tipo_expediente_id',
        'tipo_expediente',
        'id'
    ); */
}

/**
 * {@inheritdoc}
 */
public function safeDown()
{
    $this->dropTable('{{%expediente}}');
}
}
