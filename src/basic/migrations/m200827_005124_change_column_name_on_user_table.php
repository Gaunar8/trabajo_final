<?php

use yii\db\Migration;

/**
 * Class m200827_005124_change_column_name_on_user_table
 */
class m200827_005124_change_column_name_on_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('user', 'authKey', 'auth_key');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('user', 'auth_key', 'authKey');
    }
}
