<?php

use yii\db\Migration;

/**
 * Class m200827_001603_change_accessToken_attribute_from_user_table
 */
class m200827_001603_change_accessToken_attribute_from_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('user', 'accessToken', 'access_token');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('user', 'access_token', 'accessToken');
    }

}
