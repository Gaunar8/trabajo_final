<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%movimiento}}`.
 */
class m200608_015557_create_movimiento_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%movimiento}}', [
            'id' => $this->primaryKey(),
            'expediente_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'area_id' => $this->integer()->notNull(),
            'motivo' => $this->string(),
            //fecha de inicio puede setearse con posteridad?
            'fecha_inicio' => $this->datetime(),
            'fecha_fin' => $this->datetime(),
            'create_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%movimiento}}');
    }
}
