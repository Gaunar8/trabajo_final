<?php

use yii\db\Migration;

/**
 * Class m200619_225740_add_fk_to_expediente_table
 */
class m200619_225740_add_fk_to_expediente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // crea indice para columna `user_id`
    $this->createIndex(
        'idx-expediente-user_id',
        'expediente',
        'user_id'
    );

    // crea la clave foranea para la tabla `user`
    $this->addForeignKey(
        'fk-expediente-user_id',
        'expediente',
        'user_id',
        'user',
        'id'
    );
    

    // crea indice para columna `tipo_expediente_id`
    $this->createIndex(
        'idx-expediente-tipo_expediente_id',
        'expediente',
        'tipo_expediente_id'
    );

    // agrega clave foranea para tabla `tipo_expediente`
    $this->addForeignKey(
        'fk-expediente-tipo_expediente_id',
        'expediente',
        'tipo_expediente_id',
        'tipo_expediente',
        'id'
    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         // elimina la clave foranea de la tabla `expediente`
    $this->dropForeignKey(
        'fk-expediente-tipo_expediente_id',
        'expediente'
    );

    // elimina el indice de la columna `tipo_expediente_id`
    $this->dropIndex(
        'idx-expediente-tipo_expediente_id',
        'expediente'
    );

    // elimina la clave foranea de la tabla `user`
    $this->dropForeignKey(
        'fk-expediente-user_id',
        'expediente'
    );

    // elimina el indice de la columna `user_id`
    $this->dropIndex(
        'idx-expediente-user_id',
        'expediente'
    );
    }
}
