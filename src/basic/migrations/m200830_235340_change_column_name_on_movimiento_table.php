<?php

use yii\db\Migration;

/**
 * Class m200830_235340_change_column_name_on_movimiento_table
 */
class m200830_235340_change_column_name_on_movimiento_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('movimiento', 'create_at', 'created_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('movimiento', 'created_at', 'create_at');
    }
}
