<?php

use yii\db\Migration;

/**
 * Class m200620_134211_add_fk_area_table
 */
class m200620_134211_add_fk_area_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
     // crea indice para columna `user_id`a
    $this->createIndex(
        'idx-area-user_id',
        'area',
        'user_id'
    );

    // crea la clave foranea para la tabla `user`
    $this->addForeignKey(
        'fk-area-user_id',
        'area',
        'user_id',
        'user',
        'id'
    );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
          // drops foreign key for table `area`
        $this->dropForeignKey(
            'fk-area-user_id',
            'area'
        );
         // drops index for column `area`
        $this->dropIndex(
            'idx-area-user_id',
            'area'
        );
    }
}
