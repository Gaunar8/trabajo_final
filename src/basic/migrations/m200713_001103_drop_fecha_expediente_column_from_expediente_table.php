<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%expediente}}`.
 */
class m200713_001103_drop_fecha_expediente_column_from_expediente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%expediente}}', 'fecha_expediente');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%expediente}}', 'fecha_expediente', $this->date());
    }
}
