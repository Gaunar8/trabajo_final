<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%area}}`.
 */
class m200607_170910_create_area_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%area}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'nombre' => $this->string()->notNull()->unique(),
            'descripcion' => $this->string(),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'delete_at' => $this->datetime(),
        ]);
        }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%area}}');
    }
}
