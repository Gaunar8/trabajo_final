<?php

use yii\db\Migration;

/**
 * Class m200827_005150_insert_into_user_table
 */
class m200827_005150_insert_into_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'username' => 'admin',
            'area_id' => 1,
            'auth_key' => 'KjKLQodF4yN3FNnGhpXgY5Am8fyeIAhq',
            'password_hash' => '$2y$13$/cYHGAsQm14B7y399wWK.OoPRZRkbKUx1g/WN/FXcupdMK9loGxza'
        ]);
    }
}
