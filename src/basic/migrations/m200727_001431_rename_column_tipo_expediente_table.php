<?php

use yii\db\Migration;

/**
 * Class m200727_001431_rename_column_tipo_expediente_table
 */
class m200727_001431_rename_column_tipo_expediente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('tipo_expediente', 'tipo', 'nombre');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('tipo_expediente', 'nombre', 'tipo');
    }
}
