<?php

use yii\db\Migration;

/**
 * Class m200831_033956_add_descripcion_column_tipo_expediente_table
 */
class m200831_033956_add_descripcion_column_tipo_expediente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tipo_expediente}}', 'descripcion', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%tipo_expediente}}', 'descripcion');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_033956_add_descripcion_column_tipo_expediente_table cannot be reverted.\n";

        return false;
    }
    */
}
