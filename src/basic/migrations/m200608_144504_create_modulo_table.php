<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%modulo}}`.
 */
class m200608_144504_create_modulo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%modulo}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'descripcion' => $this->string(),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'delete_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%modulo}}');
    }
}
