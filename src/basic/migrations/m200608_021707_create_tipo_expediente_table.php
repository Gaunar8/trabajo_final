<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tipo_expediente}}`.
 */
class m200608_021707_create_tipo_expediente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tipo_expediente}}', [
            'id' => $this->primaryKey(),
            'tipo' => $this->string(),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'delete_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tipo_expediente}}');
    }
}
