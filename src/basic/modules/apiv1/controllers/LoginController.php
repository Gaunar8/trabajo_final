<?php

namespace app\modules\apiv1\controllers;

use app\modules\apiv1\models\User;
use yii\web\HttpException;

class LoginController extends \yii\rest\Controller {

    /**
     * {@inheritdoc}
     */
    public function actionIndex() {
        $frmusername = \Yii::$app->request->post('frmusername');
        $frmpassword = \Yii::$app->request->post('frmpassword');

        if (($user = User::findOne(['username' => $frmusername])) != null) {
            //El usuario existe, se comprueba el password
            if (!empty($frmpassword) && $user->login($frmpassword)) {
                //Generar el token de acceso y guardarlo en DB
                $user->generateToken();
                $user->save(false);

                //Generar un array con los valores de Ingreso
                $identity['username'] = $user->username;
                $identity['access_token'] = $user->access_token;
                $identity['token_expired'] = $user->token_expired;

                return $identity;
            } //Password incorrecta
        } //No existe el usuario

        throw new HttpException(401, 'Usuario o Password incorrecto');
    }

}
