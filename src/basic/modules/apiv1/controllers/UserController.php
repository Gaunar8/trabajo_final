<?php

namespace app\modules\apiv1\controllers;

use yii\rest\ActiveController;

/**
 * Default controller for the `apiv1` module
 */
class UserController extends ActiveController
{
    public $modelClass ='app\modules\apiv1\models\User';
    public function actions()
    {
        $actions = parent::actions();
        //Cambiar la fuente de datos de la accion index
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new \app\modules\apiv1\models\UserSearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
