<?php

namespace app\modules\apiv1\controllers;

use yii\rest\ActiveController;

/**
 * Default controller for the `apiv1` module
 */
class TipoexpedienteController extends ActiveController
{
    public $modelClass ='app\modules\apiv1\models\TipoExpediente';
    public function actions()
    {
        $actions = parent::actions();
        //Cambiar la fuente de datos de la accion index
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new \app\modules\apiv1\models\TipoExpedienteSearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
