<?php

namespace app\modules\apiv1\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * @property string $password Almacena la contraseña temporal
 * @property string $password_confirm Almacena la confirmacion de la contraseña temporal 
 */

class User extends \app\models\User
{
    public $password;
    public $password_confirm;
    public $password_old;

    protected $endpointName = 'user';
    protected $icon = '<i class="fas fa-users"></i>';
    protected $crudName = 'Usuarios';

    public function getCrudName(){
        return $this->crudName;
    }

    public function getIcon(){
        return $this->icon;
    }

    public function endpointName(){
        return $this->endpointName;
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    /* public function rules()
    {
        return [
            [['username'], 'required', 'on' => ['new_user']],
            [['username'], 'unique', 'targetClass' => '\app\modules\apiv1\models\User', 'message' => 'El nombre de usuario ya existe.', 'on' => ['new_user']],
            ['password_old', 'required', 'on' => ['update_password']],
            ['password_old', 'validateOldPassword', 'on' => ['update_password']],
            [['password', 'password_confirm'], 'required'],
            ['password', 'compare', 'compareAttribute' => 'password_confirm'],
            ['password', 'string', 'length' => [6, 24]],
            ['password', 'validatePassword'],
        ];
    } */

    /**
     * {@inheritdoc}
     */
   /*  public function attributeLabels()
    {
        return [
            'password_old' => 'Contraseña actual',
            'password' => 'Nueva contraseña',
            'password_confirm' => 'Confirmación de Contraseña',
            'username' => 'Nombre de Usuario'
        ];
    }
 */
    /**
     * @return Array Listado de campos a mostrar
     */
    public function fields()
    {
        return [
            'id',
            'username'
        ];
    }

    /**
     * @return Array Listado de campos opcionales
     */
    public function extraFields()
    {
        return [
            'roles' => function () {
                //Buscar a partir del AuthManager el listado de roles asignados (RBAC)
                return \Yii::$app->authManager->getRolesByUser($this->id);
            },
            'area'
        ];
    }

     /**
     * Validacion de seguridad de la nueva contraseña
     */
    public function validateOldPassword($attribute, $params, $validator)
    {
        if (!\Yii::$app->getSecurity()->validatePassword($this->$attribute, $this->password_hash)) {
            //Contraseña actual incorrecta
            $this->addError($attribute, 'La contraseña actual es incorrecta');
        }
    }

    /**
     * Validacion de seguridad de la nueva contraseña
     */
   /*  public function validatePassword($attribute, $params, $validator)
    {
        if ($this->id == 1 && \Yii::$app->user->id != 1) {
            //Si es el usuario a modificar es el adminsitrador, y lo realiza otro usuario
            $this->addError($attribute, 'El usuario administrador no puede modificarse');
        }

        $uppercase = preg_match('@[A-Z]@', $this->$attribute);
        $lowercase = preg_match('@[a-z]@', $this->$attribute);
        $number    = preg_match('@[0-9]@', $this->$attribute);

        if (!$uppercase || !$lowercase || !$number) {
            $this->addError($attribute, 'La contraseña debe incluir al menos una letra en mayúscula, una en minúscula y un número');
        }
    } */

    /**
     * @return bool true if username and password match
     */
    public function login($password)
    {
        if (\Yii::$app->getSecurity()->validatePassword($password, $this->password_hash)) {
            //Usuario y pass correctos
            return true;
        }
        //El usuario existe pero la contraseña es incorrecta
        return false;
    }
}
