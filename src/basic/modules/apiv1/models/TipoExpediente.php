<?php

namespace app\modules\apiv1\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class TipoExpediente extends \app\models\TipoExpediente
{
    protected $endpointName = 'tipoexpediente';
    protected $icon = '<i class="fas fa-clipboard-list"></i>';
    protected $crudName = 'Tipos de Expedientes';

    public function getCrudName(){
        return $this->crudName;
    }

    public function getIcon(){
        return $this->icon;
    }

    public function endpointName(){
        return $this->endpointName;
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
