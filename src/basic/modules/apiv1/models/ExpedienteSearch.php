<?php

namespace app\modules\apiv1\models;

//use yii\base\Model;
use yii\data\ActiveDataProvider;
//use app\models\Expediente;


/**
 * ExpedienteSearch represents the model behind the search form of `app\models\Expediente`.
 */
class ExpedienteSearch extends Expediente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'tipo_expediente_id', 'fojas'], 'integer'],
            [['id','estado', 'nro_expediente', 'nombre', 'apellido', 'caratula'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Expediente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params,'');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'id', $this->id])
        ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'nro_expediente', $this->nro_expediente])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido', $this->apellido])
            ->andFilterWhere(['like', 'caratula', $this->caratula]);

        return $dataProvider;
    }
}
