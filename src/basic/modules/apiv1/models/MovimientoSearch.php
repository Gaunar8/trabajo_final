<?php

//namespace app\models;
namespace app\modules\apiv1\models;
//use yii\base\Model;
use yii\data\ActiveDataProvider;
//use app\models\Movimiento;

/**
 * MovimientoSearch represents the model behind the search form of `app\models\Movimiento`.
 */
class MovimientoSearch extends Movimiento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'expediente_id', 'user_id', 'area_id'], 'integer'],
            [['motivo', 'fecha_inicio', 'fecha_fin', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Movimiento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params,'');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'id', $this->id])
        ->andFilterWhere(['like', 'expediente_id', $this->expediente_id])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'area_id', $this->area_id])
            ->andFilterWhere(['like', 'fecha_inicio', $this->fecha_inicio])
            ->andFilterWhere(['like', 'fecha_fin', $this->fecha_fin]);

        // grid filtering conditions
       /*  $query->andFilterWhere([
            'id' => $this->id,
            'expediente_id' => $this->expediente_id,
            'user_id' => $this->user_id,
            'area_id' => $this->area_id,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'motivo', $this->motivo]); */

        return $dataProvider;
    }
}
