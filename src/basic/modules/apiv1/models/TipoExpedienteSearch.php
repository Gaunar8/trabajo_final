<?php


namespace app\modules\apiv1\models;

//use yii\base\Model;
use yii\data\ActiveDataProvider;
//use app\models\TipoExpediente;

/**
 * TipoExpedienteSearch represents the model behind the search form of `app\models\TipoExpediente`.
 */
class TipoExpedienteSearch extends TipoExpediente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nombre','descripcion', 'created_at', 'updated_at', 'delete_at'], 'safe'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TipoExpediente::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params,'');

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere(['like', 'nombre', $this->nombre])
              ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
