<?php

namespace app\modules\apiv1\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Area extends \app\models\Area
{
    protected $endpointName = 'area';
    protected $icon = '<i class="far fa-object-group"></i>';
    protected $crudName = 'Areas';

    public function getCrudName(){
        return $this->crudName;
    }

    public function getIcon(){
        return $this->icon;
    }
    
    public function endpointName(){
        return $this->endpointName;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
