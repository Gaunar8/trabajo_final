<?php

namespace app\modules\apiv1\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Expediente extends \app\models\Expediente
{
    protected $endpointName = 'expediente';
    protected $icon = '<i class="fas fa-book"></i>';
    protected $crudName = 'Expedientes';

    public function getCrudName(){
        return $this->crudName;
    }

    public function getIcon(){
        return $this->icon;
    }

    public function endpointName(){
        return $this->endpointName;
    }

     /**
     * @return Array Listado de campos a mostrar
     */
    public function fields()
    {
        return [
            'id',
            'user_id',
            'tipo_expediente_id',
            'estado',
            'nro_expediente',
            'nombre',
            'apellido',
            'caratula',
            'fojas'
        ];
    }

    public function extraFields(){
        return [
            'movimientos'
        ];
    }

        /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
