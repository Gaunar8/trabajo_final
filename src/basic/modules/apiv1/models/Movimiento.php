<?php

namespace app\modules\apiv1\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Movimiento extends \app\models\Movimiento
{
    protected $endpointName = 'movimiento';
    protected $icon = '<i class="fas fa-file-import"></i>';
    protected $crudName = 'Movimientos';

    public function getCrudName(){
        return $this->crudName;
    }

    public function getIcon(){
        return $this->icon;
    }

    public function endpointName(){
        return $this->endpointName;
    }

    public function fields(){
        return [
            'id',
        ];
    }

    public function extraFields(){
        return [
            'area',
            'user',
            'expediente',
            'motivo'];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
