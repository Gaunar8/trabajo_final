<?php

namespace app\modules\apiv1\models;

use Yii;
/* use app\models\User; */
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class User extends \app\models\User
{
    protected $endpointName = 'user';
    protected $icon = '<i class="fas fa-users"></i>';
    protected $crudName = 'Usuarios';

    public function getCrudName(){
        return $this->crudName;
    }

    public function getIcon(){
        return $this->icon;
    }

    public function endpointName(){
        return $this->endpointName;
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
