<?php


namespace app\controllers;

use app\modules\apiv1\models\Expediente;
use app\models\TipoExpediente;
use yii\web\Controller;

class ExpedienteController extends Controller
{
    public function actionIndex(){
        $this->layout = 'main';
        $model = new Expediente();
        return $this->render('index',['model'=>$model]);
    }
}