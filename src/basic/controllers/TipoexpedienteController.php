<?php


namespace app\controllers;

use app\modules\apiv1\models\TipoExpediente;
use yii\web\Controller;

class TipoexpedienteController extends Controller
{
    public function actionIndex(){
        $this->layout = 'main';
        $model = new TipoExpediente();
        return $this->render('index',['model'=>$model]);
    }
}