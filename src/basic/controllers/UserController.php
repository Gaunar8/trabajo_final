<?php


namespace app\controllers;

use app\modules\apiv1\models\User;
use app\models\Area;
use yii\web\Controller;

class UserController extends Controller
{
    public function actionIndex(){
        $areas = [];
        $this->layout = 'main';
        $model = new User();
        return $this->render('index',['model'=>$model]);
    }
}