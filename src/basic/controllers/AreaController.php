<?php

use mPDF;
namespace app\controllers;

use app\modules\apiv1\models\Area;
use yii\web\Controller;

class AreaController extends Controller
{
    public function actionIndex(){
        $this->layout = 'main';
        $model = new Area();
        return $this->render('index',['model'=>$model]);
    }
}