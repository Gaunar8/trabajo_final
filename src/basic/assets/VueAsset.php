<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class VueAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//unpkg.com/bootstrap/dist/css/bootstrap.min.css',
        '//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css',
        'css/vue-suggestion.css',
        '//fonts.googleapis.com/css2?family=Fira+Sans&display=swap',
        'css/site.css?1.2',
    ];
    public $js = [
        '//unpkg.com/vue@2.6.11/dist/vue.js',
        '//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js',
        '//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue-icons.min.js',
        '//unpkg.com/axios/dist/axios.min.js',
        'js/api.js?1.1',
        '//unpkg.com/vue-suggestion',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}

