/**
 * @param String verbo
 * @param String endpoint
 * @param Array parametros
 * @param Array data
 * @returns Objet Promise
 */

async function api(verbo, endpoint, parametros, data) {
  try {
    const response = await axios({
      method: verbo,
      url: "/apiv1/" + endpoint,
      data: data, //Datos a enviar en el body
      params: parametros, //Parametros de la url
      headers: {
        "X-Api-Key": localStorage.access_token,
      },
    })
      .then((response) => {
        return {
          success: true,
          message: response.statusText,
          data: response.data,
          records: response.headers["x-pagination-total-count"],
        };
      })
      .catch(function (error) {
        let result = {};
        
        //Verificar el tipo de error
        if (error.response) {
          // Se proceso la solicitud, pero se recibio un codigo de error
          if (error.response.status === 401) {
            //La sesion expiro
            localStorage.clear();
            alert("Su sesión expiró, ingrese nuevamente.");
            location.href = '/login' //Redireccionar al login
          } else if (error.response.status === 403) {
            //No tiene permisos
            result.message =
              "Error 403: No posee permisos para ejecutar la acción solicitada.";
          } else if (error.response.status === 422) {
            //Error de validacion
            let errores = error.response.data;
            let msj = ''
            for (var i = 0; i < errores.length; i++) {
              msj += errores[i].message + " \n"
            }
            result.message = msj;
          } else if (error.response.status < 500) {
            // El error esta en el rango 4xx
            result.message = error.response.data.message;
          } else {
            // El error esta en el rango 5xx
            result.message =
              "Error " +
              error.response.status +
              ": " +
              error.response.statusText;
          }
        } else if (error.request) {
          // No se recibió respuesta desde el servidor
          result.message =
            "No se pudo procesar la consulta, el servidor no responde.";
        } else {
          // No se puedo ejecutar la consulta
          result.message = "Ocurrió un error, no se pudo ejecutar la consulta.";
        }
        result.success = false;
        return result;
      });

    return response;
  } catch (error) {
    console.error(error);
  }
}
